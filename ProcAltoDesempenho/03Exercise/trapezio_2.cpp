#include <iostream>
#include <mpi.h>
using namespace std;

double f (double);
double Trap(double, double, int, double);

int main(int argc, char** argv) {

	double integral, a = 0.0, b = 24.0, h, x;
	int n, source;
	double local_a, local_b, total;
	int local_n, rank, p;
	
	double ti, tf;
	
	//cout << "Intervalo integracao a: ";
	//cin >> a;
	
	//cout << "Intervalo integracao b: ";
	//cin >> b;
	
	//cout << "Numero de trapezios: ";
	//cin >> n;
	
	n = 2048000000;
	
	MPI_Status status;
	MPI_Init(&argc, &argv);
	
	ti = MPI_Wtime();
	
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	
	h = (b-a)/n;
	local_n = n/p;
	local_a = a + rank*local_n*h;
	local_b = local_a + local_n*h;
	integral = Trap(local_a, local_b, local_n, h);
	
	if(rank == 0){
		total = integral;
		for(source=1; source<p; source++){
			MPI_Recv(&integral, 1, MPI_DOUBLE, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
			total = total + integral;
		}
		
		printf("Com %d trapezios, a estimativa da integral de %.2f a %.2f e: %.2f \n", n, a, b, total);
		
	} else {
		MPI_Send(&integral, 1, MPI_DOUBLE, status.MPI_SOURCE, 0, MPI_COMM_WORLD);
	}
	
	tf = MPI_Wtime();
	
	printf("Com uso do MPI_ANY_SOURCE\n\n");
	printf("Tempo medido com MPI_Wtime: %1.5f\n", tf-ti);
	
	
	MPI_Finalize();
	return 0;
}

double f (double x) {
	return x*x + 1.0;
}

double Trap(double local_a, double local_b, int local_n, double h) {
	double integral, vart;
	int i;
	integral = (f(local_a) + f(local_b))*0.5;
	vart = local_a;
	
	for(i=1; i<=local_n-1; i++){
		vart += h;
		integral += f(vart);
	}
	integral *= h;
	
	return integral;
}
