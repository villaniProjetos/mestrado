#include <iostream>
#include <time.h>
#include <vector>
using namespace std;

int main(){
	
	const int N = 4096;
	int i, j;
	double somaD = 0.0;
 
	static double A[N][N];
	static double B[N][N];
	static double C[N][N];
	static double D[N][N]; 
 
 	/*
	static vector< vector<double> > A(N, vector<double> (N));
	static vector< vector<double> > B(N, vector<double> (N));
	static vector< vector<double> > C(N, vector<double> (N));
	static vector< vector<double> > D(N, vector<double> (N));
    */
    
    for(i=0; i<N; i++) {
		for(j=0; j<N; j++) {
			A[i][j] = double(i)*double(j)*1.;
			B[i][j] = double(i)*double(j)*2.;
			C[i][j] = double(i)*double(j)*5.;
			D[i][j] = 0.0;
		}
	}
    
    for(i=0; i<N; i++) {
		for(j=0; j<N; j++) {
			A[i][j+1] = B[i][j] + C[i][j];
			D[i][j] = A[i][j] * 2.;
			somaD = somaD + D[i][j];
		}
	}
	
	cout << "----- Laco Linha > Coluna -----" << endl;
	
	cout << "Soma matriz D: " << somaD << " " << endl;

    return 0;
}
