#include <iostream>
#include <time.h>
#include <vector>
using namespace std;

int main(){
	
	clock_t t1,t2;
	t1=clock();
	
	const int N = 4096;
	int i, j;

	vector<vector<double>> A(N, std::vector<double> (N));
	vector<vector<double>> B(N, std::vector<double> (N));
	vector<vector<double>> C(N, std::vector<double> (N));
	vector<vector<double>> D(N, std::vector<double> (N));
    
    for(i=0; i<N; i++) {
		for(j=0; j<N; j++) {
			A[i][j] = double(i)*double(j)*1.;
			B[i][j] = double(i)*double(j)*2.;
			C[i][j] = double(i)*double(j)*5.;
		}
	}
    
    for(j=0; j<N-1; j++) {
		for(i=0; i<N-1; i++) {
			A[i][j+1] = B[i][j] + C[i][j];
			D[i][j] = A[i][j] * 2.;
		}
	}
	
	for (i = 0; i < N; i++){	
		for (j = 0; j < N; j++){
			cout << fixed << "D[" << i << "][" << j << "]: " << D[i][j] << " " << endl;
		}
	}
	
	t2=clock();
    float diff ((float)t2-(float)t1);
    cout << "Tempo total: " << diff / CLOCKS_PER_SEC << " segundos" << endl;
    float seconds = diff / CLOCKS_PER_SEC;
	
    return 0;
}
