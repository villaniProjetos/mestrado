#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
using namespace std;

double f(double);

int main(int argc, char** argv) {

	double integral, a = 0.0, b = 1.0, h;
	clock_t ti,tf;
	
	int n, source;
	n  = 2048000000;
	
	h = (a-b)/n;
	
	ti=clock();
	
	for(source=1; source<n-1; source++){
		integral += f(a+source*h);
	}	
	integral += (f(a) + f(b)) / 2;
	integral *= h;
	
	tf = clock();
	
	printf("Com %d trapezios a integral estimada de %.2f a %.2f eh %.6f", n, a, b, integral);
	
    float diff((float)tf-(float)ti);
		
	printf("\nTempo medido: %1.6f\n", diff / CLOCKS_PER_SEC);
	
	return 0;
}

double f(double x) {
	return x*x + 1.0;
}
