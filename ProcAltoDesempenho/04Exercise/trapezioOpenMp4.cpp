#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;

double f(double);

int main(int argc, char** argv) {

	double integral, a = 0.0, b = 4.0, h, ti, tf;
	int n, source, numThd = 1;
	n  = 2048000000;
	
	h = (a-b)/n;
	
	ti = omp_get_wtime();
	
	//#pragma omp parallel for num_threads(numThd) reduction(+:integral)
	#pragma omp parallel for reduction(+:integral)
	for(source=1; source<n-1; source++){
		integral += f(a+source*h);
	}
	integral += (f(a) + f(b)) / 2;
	integral *= h;
	
	tf = omp_get_wtime();
	
	printf("Com %d trapezios a integral estimada de %.2f a %.2f eh %.6f", n, a, b, integral);
	//printf("\n\nNumero de threads = %d", numThd);
	printf("\n\nNumero de threads = %d", omp_get_num_threads());	
	printf("\nTempo medido com omp_get_wtime: %1.6f\n", tf-ti);
	
	return 0;
}

double f(double x) {
	return x*x + 1.0;
}
