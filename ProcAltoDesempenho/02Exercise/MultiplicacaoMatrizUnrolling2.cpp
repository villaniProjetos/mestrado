#include <iostream>
#include <time.h>
#include <iomanip>
#include <cmath>
#include <limits>
using namespace std;

int main(){
	
	const int N = 2048;
	const int unRl = 4;
	int i, j, k, kk;
	float somaC, c1, c2, c3, c4 = 0.0;
 
	static float A[N][N];
	static float B[N][N];
	static float C[N][N];
  
    for(i=0; i<N; i++) {
		for(j=0; j<N; j++) {
			A[i][j] = float(i)*float(j)/3.e9;
			B[i][j] = float(i)*float(j)/5.e9;
			C[i][j] = 0.0;
		}
	}
     
    for(i=0; i<N; i++) {
		for(j=0; j<N; j++) {
    
	    kk = N / unRl;
	    for(k=0; k<kk; k++) {
			C[i][j] = C[i][j] + A[k][j] * B[i][k];
		}
		
		c1 = 0.0;
		c2 = 0.0;
		c3 = 0.0;
		c4 = 0.0;
		for(k=1+kk; k<N; k+=unRl) {
			c1 = c1 + A[k][j] * B[i][k];
			c2 = c2 + A[k+1][j] * B[i][k+1];
			c3 = c3 + A[k+2][j] * B[i][k+2];
			c4 = c4 + A[k+3][j] * B[i][k+3];
		}
	    C[i][j] = C[i][j] + c1 + c2 + c3 + c4;
	    somaC = somaC + C[i][j];
		}
	}
	
	
	cout << "----- Multiplicacao COM Unrolling -----" << endl;
	
	cout << "Soma matriz C: " << setprecision(18) << somaC << " " << endl;

    return 0;
}
