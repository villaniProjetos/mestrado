#include <iostream>
#include <time.h> 
#include <iomanip>
#include <cmath>
#include <limits>
using namespace std;

int main(){
	
	const int N = 2048;
	int i, j, k;
	double somaC = 0.0;
 
	static double A[N][N];
	static double B[N][N];
	static double C[N][N];
  
    for(i=0; i<N; i++) {
		for(j=0; j<N; j++) {
			A[i][j] = double(i)*double(j)/3.e9;
			B[i][j] = double(i)*double(j)/5.e9;
			C[i][j] = 0.0;
		}
	}
    
    for(j=0; j<N; j++) {
		for(i=0; i<N; i++) {
			for(k=0; k<N; k++) {
				C[i][j] = C[i][j] + A[i][k] * B[k][j];
			}
		}
	}
    
	for(i=0; i<N; i++) {
		for(j=0; j<N; j++) {
			somaC = somaC + C[i][j];
		}
	}
		
	cout << "----- Multiplicacao SEM Unrolling -----" << endl;
	
	cout << "Soma matriz C: " << setprecision(18) << somaC << " " << endl;

    return 0;
}
