#include <iostream>
#include <time.h>
#include <iomanip>
#include <cmath>
#include <limits>
using namespace std;

int main(){
	
	const int N = 2048;
	const int unRl = 4;
	int i, j, k, kk;
	float somaC, c1, c2, c3, c4 = 0.0;
 
	static float A[N][N];
	static float B[N][N];
	static float C[N][N];
  	
  	kk = N / unRl;
  	
    for(i=0; i<N; i++) {
    	
    	for(j=0; j<kk; j++) {
			A[i][j] = float(i)*float(j)/3.e9;
			B[i][j] = float(i)*float(j)/5.e9;
			C[i][j] = 0.0;
		}
    	    	
		for(j=1+kk; j<N; j+=unRl) {
			A[i][j] = float(i)*float(j)/3.e9;
			A[i][j+1] = float(i)*float(j)/3.e9;
			A[i][j+2] = float(i)*float(j)/3.e9;
			A[i][j+3] = float(i)*float(j)/3.e9;
			
			B[i][j] = float(i)*float(j)/5.e9;
			B[i][j+1] = float(i)*float(j)/5.e9;
			B[i][j+2] = float(i)*float(j)/5.e9;
			B[i][j+3] = float(i)*float(j)/5.e9;
			
			C[i][j] = 0.0;
			C[i][j+1] = 0.0;
			C[i][j+2] = 0.0;
			C[i][j+3] = 0.0;
		}
	}
    
    for(i=0; i<N; i++) {
		for(j=0; j<N; j++) {
    
	    for(k=0; k<kk; k++) {
			C[i][j] = C[i][j] + A[i][k] * B[k][j];
		}
		
		c1 = 0.0;
		c2 = 0.0;
		c3 = 0.0;
		c4 = 0.0;
		for(k=1+kk; k<N; k+=unRl) {
			c1 = c1 + A[i][k] * B[k][j];
			c2 = c2 + A[i][k+1] * B[k+1][j];
			c3 = c3 + A[i][k+2] * B[k+2][j];
			c4 = c4 + A[i][k+3] * B[k+3][j];
		}
	    C[i][j] = C[i][j] + c1 + c2 + c3 + c4;
	    somaC = somaC + C[i][j];
		}
	}
	
	cout << "----- Multiplicacao COM Unrolling -----" << endl;
	
	cout << "Soma matriz C: " << setprecision(18) << somaC << " " << endl;

    return 0;
}
